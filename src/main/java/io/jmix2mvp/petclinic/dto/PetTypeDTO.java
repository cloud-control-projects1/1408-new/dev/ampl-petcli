package io.jmix2mvp.petclinic.dto;

import io.jmix2mvp.petclinic.entity.ProtectionStatus;

public class PetTypeDTO extends BaseDTO {
    private String name;
    private ProtectionStatus defenseStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProtectionStatus getDefenseStatus() {
        return defenseStatus;
    }

    public void setDefenseStatus(ProtectionStatus defenseStatus) {
        this.defenseStatus = defenseStatus;
    }
}
