package io.jmix2mvp.petclinic.external.petstore;

import io.jmix2mvp.petclinic.external.petstore.ApiClient;
import io.jmix2mvp.petclinic.external.petstore.api.DefaultApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Instantiates generated "petstore" OpenAPI client classes as beans.
 */
@Configuration
public class PetstoreClientConfiguration {

    @Bean("petstoreApiClient")
    public ApiClient apiClient() {
        return new ApiClient();
    }

    @Bean("petstoreDefaultApi")
    public DefaultApi defaultApi(ApiClient apiClient) {
        return new DefaultApi(apiClient);
    }
}

