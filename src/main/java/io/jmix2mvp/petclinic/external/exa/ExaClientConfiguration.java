package io.jmix2mvp.petclinic.external.exa;

import io.jmix2mvp.petclinic.external.exa.ApiClient;
import io.jmix2mvp.petclinic.external.exa.api.DefaultApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Instantiates generated "exa" OpenAPI client classes as beans.
 */
@Configuration
public class ExaClientConfiguration {

    @Bean("exaApiClient")
    public ApiClient apiClient() {
        return new ApiClient();
    }

    @Bean("exaDefaultApi")
    public DefaultApi defaultApi(ApiClient apiClient) {
        return new DefaultApi(apiClient);
    }
}

