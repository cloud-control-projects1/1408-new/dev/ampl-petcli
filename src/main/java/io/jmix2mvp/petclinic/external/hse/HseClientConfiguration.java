package io.jmix2mvp.petclinic.external.hse;

import io.jmix2mvp.petclinic.external.hse.ApiClient;
import io.jmix2mvp.petclinic.external.hse.api.AdfsApi;
import io.jmix2mvp.petclinic.external.hse.api.FeaturesApi;
import io.jmix2mvp.petclinic.external.hse.api.UserServicesApi;
import io.jmix2mvp.petclinic.external.hse.api.VirtualToursApi;
import io.jmix2mvp.petclinic.external.hse.api.PersonalCabinetApi;
import io.jmix2mvp.petclinic.external.hse.api.NotificationApi;
import io.jmix2mvp.petclinic.external.hse.api.FeedbackApi;
import io.jmix2mvp.petclinic.external.hse.api.UserInfoApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentOnlineCoursesApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentPudOverviewApi;
import io.jmix2mvp.petclinic.external.hse.api.TeacherPudOverviewApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentCalendarApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentClassroomsApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentDashboardApi;
import io.jmix2mvp.petclinic.external.hse.api.RatingsApi;
import io.jmix2mvp.petclinic.external.hse.api.LearnPlanApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentLessonApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentUserServicesShowcaseApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentCovidInfoApi;
import io.jmix2mvp.petclinic.external.hse.api.TeacherUserServicesShowcaseApi;
import io.jmix2mvp.petclinic.external.hse.api.EmployeeUserServicesShowcaseApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentConsultationApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentRecordBookApi;
import io.jmix2mvp.petclinic.external.hse.api.StudentWidgetApi;
import io.jmix2mvp.petclinic.external.hse.api.EmployeeWidgetApi;
import io.jmix2mvp.petclinic.external.hse.api.PublicMainPageApi;
import io.jmix2mvp.petclinic.external.hse.api.PublicOnlineCoursesApi;
import io.jmix2mvp.petclinic.external.hse.api.TeacherDashboardApi;
import io.jmix2mvp.petclinic.external.hse.api.TeacherWidgetApi;
import io.jmix2mvp.petclinic.external.hse.api.TeacherConsultationApi;
import io.jmix2mvp.petclinic.external.hse.api.TeacherLearnPlanApi;
import io.jmix2mvp.petclinic.external.hse.api.SevExamSheetCommonApi;
import io.jmix2mvp.petclinic.external.hse.api.SevMergedExamSheetCommonApi;
import io.jmix2mvp.petclinic.external.hse.api.SevFilterCommonApi;
import io.jmix2mvp.petclinic.external.hse.api.SevExamSheetApi;
import io.jmix2mvp.petclinic.external.hse.api.SevMergedExamSheetApi;
import io.jmix2mvp.petclinic.external.hse.api.SevTrainingOfficeManagerExamSheetApi;
import io.jmix2mvp.petclinic.external.hse.api.SevDepartmentManagerExamSheetApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerStudentOnlineCoursesApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerOffersApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerOnlineCoursesApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerContractsApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerStudentsApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerGroupsApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerContractReportsApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerOnlineCourseReportsApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerGroupReportsApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerStudentReportsApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerManagerBiApi;
import io.jmix2mvp.petclinic.external.hse.api.PartnerStudentCertificatesApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonMyApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonBulkApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonFilterApi;
import io.jmix2mvp.petclinic.external.hse.api.PublicEppFilterApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonReportApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonKindApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonTypeApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonTypeOnKindApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonReferenceMaterialApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonStudentApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonTypeOnKindOnInitiatorTypeApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonAcademicYearApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonTagApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonPrerequisiteApi;
import io.jmix2mvp.petclinic.external.hse.api.EppDoopApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationDoopApi;
import io.jmix2mvp.petclinic.external.hse.api.EppLegalEntityApi;
import io.jmix2mvp.petclinic.external.hse.api.EppEmployeeApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationEmployeeApi;
import io.jmix2mvp.petclinic.external.hse.api.EppStudentApi;
import io.jmix2mvp.petclinic.external.hse.api.EppAcademicSupervisorApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationAcademicSupervisorApi;
import io.jmix2mvp.petclinic.external.hse.api.EppFacultyProjectManagerApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationFacultyProjectManagerApi;
import io.jmix2mvp.petclinic.external.hse.api.EppDeputyDeanApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationDeputyDeanApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationCommonApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationCommonBulkApi;
import io.jmix2mvp.petclinic.external.hse.api.EppFairApi;
import io.jmix2mvp.petclinic.external.hse.api.EppCommonProjectProposalApi;
import io.jmix2mvp.petclinic.external.hse.api.EppApplicationStudentApi;
import io.jmix2mvp.petclinic.external.hse.api.EppSubscriptionStudentApi;
import io.jmix2mvp.petclinic.external.hse.api.EppContractApi;
import io.jmix2mvp.petclinic.external.hse.api.FileStorageApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Instantiates generated "hse" OpenAPI client classes as beans.
 */
@Configuration
public class HseClientConfiguration {

    @Bean("hseApiClient")
    public ApiClient apiClient() {
        return new ApiClient();
    }

    @Bean("hseAdfsApi")
    public AdfsApi adfsApi(ApiClient apiClient) {
        return new AdfsApi(apiClient);
    }

    @Bean("hseFeaturesApi")
    public FeaturesApi featuresApi(ApiClient apiClient) {
        return new FeaturesApi(apiClient);
    }

    @Bean("hseUserServicesApi")
    public UserServicesApi userServicesApi(ApiClient apiClient) {
        return new UserServicesApi(apiClient);
    }

    @Bean("hseVirtualToursApi")
    public VirtualToursApi virtualToursApi(ApiClient apiClient) {
        return new VirtualToursApi(apiClient);
    }

    @Bean("hsePersonalCabinetApi")
    public PersonalCabinetApi personalCabinetApi(ApiClient apiClient) {
        return new PersonalCabinetApi(apiClient);
    }

    @Bean("hseNotificationApi")
    public NotificationApi notificationApi(ApiClient apiClient) {
        return new NotificationApi(apiClient);
    }

    @Bean("hseFeedbackApi")
    public FeedbackApi feedbackApi(ApiClient apiClient) {
        return new FeedbackApi(apiClient);
    }

    @Bean("hseUserInfoApi")
    public UserInfoApi userInfoApi(ApiClient apiClient) {
        return new UserInfoApi(apiClient);
    }

    @Bean("hseStudentOnlineCoursesApi")
    public StudentOnlineCoursesApi studentOnlineCoursesApi(ApiClient apiClient) {
        return new StudentOnlineCoursesApi(apiClient);
    }

    @Bean("hseStudentPudOverviewApi")
    public StudentPudOverviewApi studentPudOverviewApi(ApiClient apiClient) {
        return new StudentPudOverviewApi(apiClient);
    }

    @Bean("hseTeacherPudOverviewApi")
    public TeacherPudOverviewApi teacherPudOverviewApi(ApiClient apiClient) {
        return new TeacherPudOverviewApi(apiClient);
    }

    @Bean("hseStudentCalendarApi")
    public StudentCalendarApi studentCalendarApi(ApiClient apiClient) {
        return new StudentCalendarApi(apiClient);
    }

    @Bean("hseStudentClassroomsApi")
    public StudentClassroomsApi studentClassroomsApi(ApiClient apiClient) {
        return new StudentClassroomsApi(apiClient);
    }

    @Bean("hseStudentDashboardApi")
    public StudentDashboardApi studentDashboardApi(ApiClient apiClient) {
        return new StudentDashboardApi(apiClient);
    }

    @Bean("hseRatingsApi")
    public RatingsApi ratingsApi(ApiClient apiClient) {
        return new RatingsApi(apiClient);
    }

    @Bean("hseLearnPlanApi")
    public LearnPlanApi learnPlanApi(ApiClient apiClient) {
        return new LearnPlanApi(apiClient);
    }

    @Bean("hseStudentLessonApi")
    public StudentLessonApi studentLessonApi(ApiClient apiClient) {
        return new StudentLessonApi(apiClient);
    }

    @Bean("hseStudentUserServicesShowcaseApi")
    public StudentUserServicesShowcaseApi studentUserServicesShowcaseApi(ApiClient apiClient) {
        return new StudentUserServicesShowcaseApi(apiClient);
    }

    @Bean("hseStudentCovidInfoApi")
    public StudentCovidInfoApi studentCovidInfoApi(ApiClient apiClient) {
        return new StudentCovidInfoApi(apiClient);
    }

    @Bean("hseTeacherUserServicesShowcaseApi")
    public TeacherUserServicesShowcaseApi teacherUserServicesShowcaseApi(ApiClient apiClient) {
        return new TeacherUserServicesShowcaseApi(apiClient);
    }

    @Bean("hseEmployeeUserServicesShowcaseApi")
    public EmployeeUserServicesShowcaseApi employeeUserServicesShowcaseApi(ApiClient apiClient) {
        return new EmployeeUserServicesShowcaseApi(apiClient);
    }

    @Bean("hseStudentConsultationApi")
    public StudentConsultationApi studentConsultationApi(ApiClient apiClient) {
        return new StudentConsultationApi(apiClient);
    }

    @Bean("hseStudentRecordBookApi")
    public StudentRecordBookApi studentRecordBookApi(ApiClient apiClient) {
        return new StudentRecordBookApi(apiClient);
    }

    @Bean("hseStudentWidgetApi")
    public StudentWidgetApi studentWidgetApi(ApiClient apiClient) {
        return new StudentWidgetApi(apiClient);
    }

    @Bean("hseEmployeeWidgetApi")
    public EmployeeWidgetApi employeeWidgetApi(ApiClient apiClient) {
        return new EmployeeWidgetApi(apiClient);
    }

    @Bean("hsePublicMainPageApi")
    public PublicMainPageApi publicMainPageApi(ApiClient apiClient) {
        return new PublicMainPageApi(apiClient);
    }

    @Bean("hsePublicOnlineCoursesApi")
    public PublicOnlineCoursesApi publicOnlineCoursesApi(ApiClient apiClient) {
        return new PublicOnlineCoursesApi(apiClient);
    }

    @Bean("hseTeacherDashboardApi")
    public TeacherDashboardApi teacherDashboardApi(ApiClient apiClient) {
        return new TeacherDashboardApi(apiClient);
    }

    @Bean("hseTeacherWidgetApi")
    public TeacherWidgetApi teacherWidgetApi(ApiClient apiClient) {
        return new TeacherWidgetApi(apiClient);
    }

    @Bean("hseTeacherConsultationApi")
    public TeacherConsultationApi teacherConsultationApi(ApiClient apiClient) {
        return new TeacherConsultationApi(apiClient);
    }

    @Bean("hseTeacherLearnPlanApi")
    public TeacherLearnPlanApi teacherLearnPlanApi(ApiClient apiClient) {
        return new TeacherLearnPlanApi(apiClient);
    }

    @Bean("hseSevExamSheetCommonApi")
    public SevExamSheetCommonApi sevExamSheetCommonApi(ApiClient apiClient) {
        return new SevExamSheetCommonApi(apiClient);
    }

    @Bean("hseSevMergedExamSheetCommonApi")
    public SevMergedExamSheetCommonApi sevMergedExamSheetCommonApi(ApiClient apiClient) {
        return new SevMergedExamSheetCommonApi(apiClient);
    }

    @Bean("hseSevFilterCommonApi")
    public SevFilterCommonApi sevFilterCommonApi(ApiClient apiClient) {
        return new SevFilterCommonApi(apiClient);
    }

    @Bean("hseSevExamSheetApi")
    public SevExamSheetApi sevExamSheetApi(ApiClient apiClient) {
        return new SevExamSheetApi(apiClient);
    }

    @Bean("hseSevMergedExamSheetApi")
    public SevMergedExamSheetApi sevMergedExamSheetApi(ApiClient apiClient) {
        return new SevMergedExamSheetApi(apiClient);
    }

    @Bean("hseSevTrainingOfficeManagerExamSheetApi")
    public SevTrainingOfficeManagerExamSheetApi sevTrainingOfficeManagerExamSheetApi(ApiClient apiClient) {
        return new SevTrainingOfficeManagerExamSheetApi(apiClient);
    }

    @Bean("hseSevDepartmentManagerExamSheetApi")
    public SevDepartmentManagerExamSheetApi sevDepartmentManagerExamSheetApi(ApiClient apiClient) {
        return new SevDepartmentManagerExamSheetApi(apiClient);
    }

    @Bean("hsePartnerStudentOnlineCoursesApi")
    public PartnerStudentOnlineCoursesApi partnerStudentOnlineCoursesApi(ApiClient apiClient) {
        return new PartnerStudentOnlineCoursesApi(apiClient);
    }

    @Bean("hsePartnerManagerOffersApi")
    public PartnerManagerOffersApi partnerManagerOffersApi(ApiClient apiClient) {
        return new PartnerManagerOffersApi(apiClient);
    }

    @Bean("hsePartnerManagerOnlineCoursesApi")
    public PartnerManagerOnlineCoursesApi partnerManagerOnlineCoursesApi(ApiClient apiClient) {
        return new PartnerManagerOnlineCoursesApi(apiClient);
    }

    @Bean("hsePartnerManagerContractsApi")
    public PartnerManagerContractsApi partnerManagerContractsApi(ApiClient apiClient) {
        return new PartnerManagerContractsApi(apiClient);
    }

    @Bean("hsePartnerManagerStudentsApi")
    public PartnerManagerStudentsApi partnerManagerStudentsApi(ApiClient apiClient) {
        return new PartnerManagerStudentsApi(apiClient);
    }

    @Bean("hsePartnerManagerGroupsApi")
    public PartnerManagerGroupsApi partnerManagerGroupsApi(ApiClient apiClient) {
        return new PartnerManagerGroupsApi(apiClient);
    }

    @Bean("hsePartnerManagerContractReportsApi")
    public PartnerManagerContractReportsApi partnerManagerContractReportsApi(ApiClient apiClient) {
        return new PartnerManagerContractReportsApi(apiClient);
    }

    @Bean("hsePartnerManagerOnlineCourseReportsApi")
    public PartnerManagerOnlineCourseReportsApi partnerManagerOnlineCourseReportsApi(ApiClient apiClient) {
        return new PartnerManagerOnlineCourseReportsApi(apiClient);
    }

    @Bean("hsePartnerManagerGroupReportsApi")
    public PartnerManagerGroupReportsApi partnerManagerGroupReportsApi(ApiClient apiClient) {
        return new PartnerManagerGroupReportsApi(apiClient);
    }

    @Bean("hsePartnerManagerStudentReportsApi")
    public PartnerManagerStudentReportsApi partnerManagerStudentReportsApi(ApiClient apiClient) {
        return new PartnerManagerStudentReportsApi(apiClient);
    }

    @Bean("hsePartnerManagerBiApi")
    public PartnerManagerBiApi partnerManagerBiApi(ApiClient apiClient) {
        return new PartnerManagerBiApi(apiClient);
    }

    @Bean("hsePartnerStudentCertificatesApi")
    public PartnerStudentCertificatesApi partnerStudentCertificatesApi(ApiClient apiClient) {
        return new PartnerStudentCertificatesApi(apiClient);
    }

    @Bean("hseEppCommonApi")
    public EppCommonApi eppCommonApi(ApiClient apiClient) {
        return new EppCommonApi(apiClient);
    }

    @Bean("hseEppCommonMyApi")
    public EppCommonMyApi eppCommonMyApi(ApiClient apiClient) {
        return new EppCommonMyApi(apiClient);
    }

    @Bean("hseEppCommonBulkApi")
    public EppCommonBulkApi eppCommonBulkApi(ApiClient apiClient) {
        return new EppCommonBulkApi(apiClient);
    }

    @Bean("hseEppCommonFilterApi")
    public EppCommonFilterApi eppCommonFilterApi(ApiClient apiClient) {
        return new EppCommonFilterApi(apiClient);
    }

    @Bean("hsePublicEppFilterApi")
    public PublicEppFilterApi publicEppFilterApi(ApiClient apiClient) {
        return new PublicEppFilterApi(apiClient);
    }

    @Bean("hseEppCommonReportApi")
    public EppCommonReportApi eppCommonReportApi(ApiClient apiClient) {
        return new EppCommonReportApi(apiClient);
    }

    @Bean("hseEppCommonKindApi")
    public EppCommonKindApi eppCommonKindApi(ApiClient apiClient) {
        return new EppCommonKindApi(apiClient);
    }

    @Bean("hseEppCommonTypeApi")
    public EppCommonTypeApi eppCommonTypeApi(ApiClient apiClient) {
        return new EppCommonTypeApi(apiClient);
    }

    @Bean("hseEppCommonTypeOnKindApi")
    public EppCommonTypeOnKindApi eppCommonTypeOnKindApi(ApiClient apiClient) {
        return new EppCommonTypeOnKindApi(apiClient);
    }

    @Bean("hseEppCommonReferenceMaterialApi")
    public EppCommonReferenceMaterialApi eppCommonReferenceMaterialApi(ApiClient apiClient) {
        return new EppCommonReferenceMaterialApi(apiClient);
    }

    @Bean("hseEppCommonStudentApi")
    public EppCommonStudentApi eppCommonStudentApi(ApiClient apiClient) {
        return new EppCommonStudentApi(apiClient);
    }

    @Bean("hseEppCommonTypeOnKindOnInitiatorTypeApi")
    public EppCommonTypeOnKindOnInitiatorTypeApi eppCommonTypeOnKindOnInitiatorTypeApi(ApiClient apiClient) {
        return new EppCommonTypeOnKindOnInitiatorTypeApi(apiClient);
    }

    @Bean("hseEppCommonAcademicYearApi")
    public EppCommonAcademicYearApi eppCommonAcademicYearApi(ApiClient apiClient) {
        return new EppCommonAcademicYearApi(apiClient);
    }

    @Bean("hseEppCommonTagApi")
    public EppCommonTagApi eppCommonTagApi(ApiClient apiClient) {
        return new EppCommonTagApi(apiClient);
    }

    @Bean("hseEppCommonPrerequisiteApi")
    public EppCommonPrerequisiteApi eppCommonPrerequisiteApi(ApiClient apiClient) {
        return new EppCommonPrerequisiteApi(apiClient);
    }

    @Bean("hseEppDoopApi")
    public EppDoopApi eppDoopApi(ApiClient apiClient) {
        return new EppDoopApi(apiClient);
    }

    @Bean("hseEppApplicationDoopApi")
    public EppApplicationDoopApi eppApplicationDoopApi(ApiClient apiClient) {
        return new EppApplicationDoopApi(apiClient);
    }

    @Bean("hseEppLegalEntityApi")
    public EppLegalEntityApi eppLegalEntityApi(ApiClient apiClient) {
        return new EppLegalEntityApi(apiClient);
    }

    @Bean("hseEppEmployeeApi")
    public EppEmployeeApi eppEmployeeApi(ApiClient apiClient) {
        return new EppEmployeeApi(apiClient);
    }

    @Bean("hseEppApplicationEmployeeApi")
    public EppApplicationEmployeeApi eppApplicationEmployeeApi(ApiClient apiClient) {
        return new EppApplicationEmployeeApi(apiClient);
    }

    @Bean("hseEppStudentApi")
    public EppStudentApi eppStudentApi(ApiClient apiClient) {
        return new EppStudentApi(apiClient);
    }

    @Bean("hseEppAcademicSupervisorApi")
    public EppAcademicSupervisorApi eppAcademicSupervisorApi(ApiClient apiClient) {
        return new EppAcademicSupervisorApi(apiClient);
    }

    @Bean("hseEppApplicationAcademicSupervisorApi")
    public EppApplicationAcademicSupervisorApi eppApplicationAcademicSupervisorApi(ApiClient apiClient) {
        return new EppApplicationAcademicSupervisorApi(apiClient);
    }

    @Bean("hseEppFacultyProjectManagerApi")
    public EppFacultyProjectManagerApi eppFacultyProjectManagerApi(ApiClient apiClient) {
        return new EppFacultyProjectManagerApi(apiClient);
    }

    @Bean("hseEppApplicationFacultyProjectManagerApi")
    public EppApplicationFacultyProjectManagerApi eppApplicationFacultyProjectManagerApi(ApiClient apiClient) {
        return new EppApplicationFacultyProjectManagerApi(apiClient);
    }

    @Bean("hseEppDeputyDeanApi")
    public EppDeputyDeanApi eppDeputyDeanApi(ApiClient apiClient) {
        return new EppDeputyDeanApi(apiClient);
    }

    @Bean("hseEppApplicationDeputyDeanApi")
    public EppApplicationDeputyDeanApi eppApplicationDeputyDeanApi(ApiClient apiClient) {
        return new EppApplicationDeputyDeanApi(apiClient);
    }

    @Bean("hseEppApplicationCommonApi")
    public EppApplicationCommonApi eppApplicationCommonApi(ApiClient apiClient) {
        return new EppApplicationCommonApi(apiClient);
    }

    @Bean("hseEppApplicationCommonBulkApi")
    public EppApplicationCommonBulkApi eppApplicationCommonBulkApi(ApiClient apiClient) {
        return new EppApplicationCommonBulkApi(apiClient);
    }

    @Bean("hseEppFairApi")
    public EppFairApi eppFairApi(ApiClient apiClient) {
        return new EppFairApi(apiClient);
    }

    @Bean("hseEppCommonProjectProposalApi")
    public EppCommonProjectProposalApi eppCommonProjectProposalApi(ApiClient apiClient) {
        return new EppCommonProjectProposalApi(apiClient);
    }

    @Bean("hseEppApplicationStudentApi")
    public EppApplicationStudentApi eppApplicationStudentApi(ApiClient apiClient) {
        return new EppApplicationStudentApi(apiClient);
    }

    @Bean("hseEppSubscriptionStudentApi")
    public EppSubscriptionStudentApi eppSubscriptionStudentApi(ApiClient apiClient) {
        return new EppSubscriptionStudentApi(apiClient);
    }

    @Bean("hseEppContractApi")
    public EppContractApi eppContractApi(ApiClient apiClient) {
        return new EppContractApi(apiClient);
    }

    @Bean("hseFileStorageApi")
    public FileStorageApi fileStorageApi(ApiClient apiClient) {
        return new FileStorageApi(apiClient);
    }
}

