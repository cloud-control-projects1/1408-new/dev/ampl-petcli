package io.jmix2mvp.petclinic.external.test;

import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@PropertySource("classpath:graphql-external-integration.properties")
@ConfigurationProperties(prefix = "app.graphql.external.test")
public class TestGraphQLClientConfiguration {

    private String url;

    @Bean("testGraphQLClient")
    public MonoGraphQLClient ownerGraphQLClient() {
        WebClient webClient = WebClient.builder()
                .baseUrl(getUrl())
                .build();
        return MonoGraphQLClient.createWithWebClient(webClient);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
