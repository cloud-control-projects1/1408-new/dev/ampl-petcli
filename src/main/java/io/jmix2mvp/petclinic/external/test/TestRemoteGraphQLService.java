package io.jmix2mvp.petclinic.external.test;

import com.amplicode.core.graphql.scalars.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.TypeRef;
import com.netflix.graphql.dgs.client.GraphQLError;
import com.netflix.graphql.dgs.client.GraphQLResponse;
import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import com.netflix.graphql.dgs.client.codegen.BaseProjectionNode;
import com.netflix.graphql.dgs.client.codegen.GraphQLQuery;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import graphql.scalars.ExtendedScalars;
import graphql.schema.Coercing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import io.jmix2mvp.petclinic.external.test.client.*;
import io.jmix2mvp.petclinic.external.test.types.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.time.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Entry point for the "test" GraphQL web service client.
 */
@Service
public class TestRemoteGraphQLService {

    private static final Logger log = LoggerFactory.getLogger(TestRemoteGraphQLService.class);

    protected final MonoGraphQLClient client;
    protected final ObjectMapper objectMapper;

    public TestRemoteGraphQLService(@Qualifier("testGraphQLClient") MonoGraphQLClient client, ObjectMapper objectMapper) {
        this.client = client;
        this.objectMapper = objectMapper;
    }

    /**
     * GraphQL query
     * <pre>
     * userInfo: UserInfo
     * </pre>
     */
    public UserInfo userInfo(UserInfoProjectionRoot projection) {
        GraphQLQueryRequest graphQLQueryRequest = createRequest(
                UserInfoGraphQLQuery.newRequest()
                        .build(),
                projection
        );
        GraphQLResponse graphQLResponse = executeRequest(graphQLQueryRequest);
        return graphQLResponse.extractValueAsObject("userInfo", UserInfo.class);
    }

    /**
     * GraphQL query
     * <pre>
     * userPermissions: [String]
     * </pre>
     */
    public List<String> userPermissions() {
        GraphQLQueryRequest graphQLQueryRequest = createRequest(
                UserPermissionsGraphQLQuery.newRequest()
                        .build(),
                null
        );
        GraphQLResponse graphQLResponse = executeRequest(graphQLQueryRequest);
        return graphQLResponse.extractValueAsObject("userPermissions", new TypeRef<>() {
        });
    }

    private GraphQLResponse executeRequest(GraphQLQueryRequest graphQLQueryRequest) {
        String serializedRequest = graphQLQueryRequest.serialize();
        log.debug("Request: {}", serializedRequest);

        GraphQLResponse graphQLResponse = client.reactiveExecuteQuery(serializedRequest)
                .blockOptional()
                .orElseThrow(() -> {
                    throw new RuntimeException("No response");
                });

        if (graphQLResponse.hasErrors()) {
            List<GraphQLError> errors = graphQLResponse.getErrors();
            log.error("Errors: {}", errors);
            throw new RuntimeException(errors.get(0).getMessage());
        }

        return graphQLResponse;
    }

    private GraphQLQueryRequest createRequest(GraphQLQuery query, BaseProjectionNode projection) {
        return new GraphQLQueryRequest(query, projection, getCoercingMapping());
    }

    protected Map<Class<?>, ? extends Coercing<?, ?>> getCoercingMapping() {
        return Map.of(
                BigDecimal.class, BigDecimalScalar.INSTANCE.getCoercing(),
                BigInteger.class, BigIntegerScalar.INSTANCE.getCoercing(),
                LocalDate.class, ExtendedScalars.Date.getCoercing(),
                OffsetDateTime.class, ExtendedScalars.DateTime.getCoercing(),
                LocalDateTime.class, LocalDateTimeScalar.INSTANCE.getCoercing(),
                LocalTime.class, LocalTimeScalar.INSTANCE.getCoercing(),
                Long.class, LongScalar.INSTANCE.getCoercing(),
                OffsetTime.class, ExtendedScalars.Time.getCoercing(),
                Date.class, TimestampScalar.INSTANCE.getCoercing(),
                URL.class, ExtendedScalars.Url.getCoercing()
        );
    }

}
