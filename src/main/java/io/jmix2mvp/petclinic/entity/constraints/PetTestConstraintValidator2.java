package io.jmix2mvp.petclinic.entity.constraints;

import io.jmix2mvp.petclinic.entity.Pet;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class PetTestConstraintValidator2 implements ConstraintValidator<PetTestConstraint2, Pet> {

    @Override
    public void initialize(PetTestConstraint2 constraintAnnotation) {
        // do nothing
    }

    @Override
    public boolean isValid(Pet pet, ConstraintValidatorContext constraintValidatorContext) {
        if (pet.getBirthDate() == null || pet.getIdentificationNumber() == null) {
            return true;
        }
        return !pet.getBirthDate().isBefore(LocalDate.parse("2021-01-01")) || pet.getIdentificationNumber().matches("^[a-zA-Z]*$");
    }
}
