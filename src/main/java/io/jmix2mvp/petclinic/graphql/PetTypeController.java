package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import io.jmix2mvp.petclinic.dto.PetTypeDTO;
import io.jmix2mvp.petclinic.dto.PetTypeInputDTO;
import io.jmix2mvp.petclinic.entity.PetType;
import io.jmix2mvp.petclinic.entity.ProtectionStatus;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.PetTypeRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;

@Controller
public class PetTypeController {
    private final PetTypeRepository crudRepository;
    private final DTOMapper mapper;

    public PetTypeController(PetTypeRepository petTypeRepository, DTOMapper mapper) {
        this.crudRepository = petTypeRepository;
        this.mapper = mapper;
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petType")
    @Transactional
    public PetTypeDTO findById(@GraphQLId @Argument Long id) {
        return crudRepository.findById(id)
                .map(mapper::petTypeToDTO)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));
    }

    @Secured({ADMIN, VETERINARIAN})
    @NonNull
    @QueryMapping(name = "petTypeList")
    @Transactional(readOnly = true)
    public List<PetTypeDTO> findAll(@Argument PetTypeFilter filter) {
        List<PetType> list = crudRepository.findAll(createFilter(filter));
        return list.stream()
                .map(mapper::petTypeToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petTypeListByIds")
    @Transactional
    public List<PetTypeDTO> findAllById(@NonNull @Argument List<@GraphQLId Long> ids) {
        return crudRepository.findAllById(ids).stream()
                .map(mapper::petTypeToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "updatePetType")
    @Transactional
    public PetTypeDTO update(@Argument @Valid PetTypeInputDTO input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new ResourceNotFoundException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }

        PetType entity = new PetType();
        mapper.petTypeDTOToEntity(input, entity);
        entity = crudRepository.save(entity);

        return mapper.petTypeToDTO(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deletePetType")
    @Transactional
    public void delete(@GraphQLId @NonNull @Argument Long id) {
        PetType entity = crudRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deleteManyPetType")
    @Transactional
    public void deleteAllById(@NonNull @Argument List<@GraphQLId Long> ids) {
        crudRepository.deleteAllById(ids);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "updateManyPetType")
    @Transactional
    public List<PetTypeDTO> updateMany(@NonNull @Argument List<PetTypeInputDTO> inputList) {
        List<PetType> entities = inputList.stream()
                .map(input -> {
                    PetType entity = new PetType();
                    mapper.petTypeDTOToEntity(input, entity);
                    return entity;
                })
                .collect(Collectors.toList());

        entities = crudRepository.saveAllAndFlush(entities);

        return entities.stream().map(mapper::petTypeToDTO)
                .collect(Collectors.toList());
    }

    protected Specification<PetType> createFilter(PetTypeFilter filter) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (filter.name != null) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + filter.name.toLowerCase() + "%"));
                }
                if (filter.defenseStatus != null) {
                    predicates.add(criteriaBuilder.equal(root.get("defenseStatus"), filter.defenseStatus));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    static class PetTypeFilter {

        private String name;
        private ProtectionStatus defenseStatus;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ProtectionStatus getDefenseStatus() {
            return defenseStatus;
        }

        public void setDefenseStatus(ProtectionStatus defenseStatus) {
            this.defenseStatus = defenseStatus;
        }
    }
}
