package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import io.jmix2mvp.petclinic.dto.PetDescriptionDTO;
import io.jmix2mvp.petclinic.dto.PetDescriptionInputDTO;
import io.jmix2mvp.petclinic.entity.PetDescription;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.PetDescriptionRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;

@Controller
public class PetDescriptionController {
    private final PetDescriptionRepository crudRepository;
    private final DTOMapper mapper;

    public PetDescriptionController(PetDescriptionRepository crudRepository, DTOMapper mapper) {
        this.crudRepository = crudRepository;
        this.mapper = mapper;
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deletePetDescription")
    @Transactional
    public void delete(@GraphQLId @Argument @NonNull Long id) {
        PetDescription entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petDescriptionList")
    @Transactional(readOnly = true)
    @NonNull
    public List<PetDescriptionDTO> findAll() {
        return crudRepository.findAll().stream()
                .map(mapper::petDescriptionToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petDescription")
    @Transactional(readOnly = true)
    @NonNull
    public PetDescriptionDTO findById(@GraphQLId @Argument @NonNull Long id) {
        return crudRepository.findById(id)
                .map(mapper::petDescriptionToDTO)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "updatePetDescription")
    @Transactional
    @NonNull
    public PetDescriptionDTO update(@Argument @NonNull PetDescriptionInputDTO input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        PetDescription entity = new PetDescription();
        mapper.updatePetDescriptionFromPetDescriptionInputDTO(input, entity);
        entity = crudRepository.save(entity);
        return mapper.petDescriptionToDTO(entity);
    }
}