package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import com.amplicode.core.graphql.paging.OffsetPageInput;
import com.amplicode.core.graphql.paging.ResultPage;
import io.jmix2mvp.petclinic.dto.NotNullScalarsTestEntityFilter;
import io.jmix2mvp.petclinic.entity.NotNullScalarsTestEntity;
import io.jmix2mvp.petclinic.repository.NotNullScalarsTestEntityRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class NotNullScalarsTestEntityController {
    private final NotNullScalarsTestEntityRepository crudRepository;

    public NotNullScalarsTestEntityController(NotNullScalarsTestEntityRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @MutationMapping(name = "deleteNotNullScalarsTestEntity")
    @Transactional
    public void delete(@GraphQLId @Argument Long id) {
        NotNullScalarsTestEntity entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @QueryMapping(name = "notNullScalarsTestEntityListFull")
    @Transactional
    public List<NotNullScalarsTestEntity> findAll() {
        return crudRepository.findAll();
    }

    @QueryMapping(name = "notNullScalarsTestEntity")
    @Transactional
    public NotNullScalarsTestEntity findById(@GraphQLId @Argument Long id) {
        return crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @MutationMapping(name = "updateNotNullScalarsTestEntity")
    @Transactional
    public NotNullScalarsTestEntity update(@Argument NotNullScalarsTestEntity input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        return crudRepository.save(input);
    }


    @NonNull
    @QueryMapping(name = "notNullScalarsTestEntityList")
    public ResultPage<NotNullScalarsTestEntity> notNullScalarsTestEntityList(@Argument OffsetPageInput page, @Argument List<NotNullScalarsTestEntityOrderByInput> sort, @Argument NotNullScalarsTestEntityFilter filter) {
        Pageable pageable = Optional.ofNullable(page)
                .map(p -> PageRequest.of(p.getNumber(), p.getSize()).withSort(createSort(sort)))
                .orElseGet(() -> PageRequest.ofSize(20).withSort(createSort(sort)));
        Page<NotNullScalarsTestEntity> result = crudRepository.findAll(createFilter(filter), pageable);
        return ResultPage.page(result.getContent(), result.getTotalElements());
    }

    protected Sort createSort(List<NotNullScalarsTestEntityOrderByInput> sortInput) {
        if (sortInput == null || sortInput.isEmpty()) {
            return Sort.unsorted();
        }
        List<Sort.Order> orders = sortInput.stream()
                .map(item -> {
                    Sort.Direction direction;
                    if (item.getDirection() == SortDirection.ASC) {
                        direction = Sort.Direction.ASC;
                    } else {
                        direction = Sort.Direction.DESC;
                    }
                    switch (item.getProperty()) {
                        case STRING_NOT_NULL:
                            return Sort.Order.by("stringNotNull").with(direction);
                        case BIG_INT_NOT_NULL:
                            return Sort.Order.by("bigIntNotNull").with(direction);
                        case BIG_DECIMAL_NOT_NULL:
                            return Sort.Order.by("bigDecimalNotNull").with(direction);
                        case LOCAL_DATE_NOT_NULL:
                            return Sort.Order.by("localDateNotNull").with(direction);
                        case OFFSET_TIME_NOT_NULL:
                            return Sort.Order.by("offsetTimeNotNull").with(direction);
                        case OFFSET_DATE_TIME_NOT_NULL:
                            return Sort.Order.by("offsetDateTimeNotNull").with(direction);
                        case LOCAL_TIME_NOT_NULL:
                            return Sort.Order.by("localTimeNotNull").with(direction);
                        case LOCAL_DATE_TIME_NOT_NULL:
                            return Sort.Order.by("localDateTimeNotNull").with(direction);
                        case DATE_TEST_NOT_NULL:
                            return Sort.Order.by("dateTestNotNull").with(direction);
                        case URL_NOT_NULL:
                            return Sort.Order.by("urlNotNull").with(direction);
                        default:
                            return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return Sort.by(orders);
    }

    static class NotNullScalarsTestEntityOrderByInput {

        private NotNullScalarsTestEntityOrderByProperty property;
        private SortDirection direction;

        public NotNullScalarsTestEntityOrderByProperty getProperty() {
            return property;
        }

        public void setProperty(NotNullScalarsTestEntityOrderByProperty property) {
            this.property = property;
        }

        public SortDirection getDirection() {
            return direction;
        }

        public void setDirection(SortDirection direction) {
            this.direction = direction;
        }
    }

    public enum SortDirection {ASC, DESC}

    public enum NotNullScalarsTestEntityOrderByProperty {STRING_NOT_NULL, BIG_INT_NOT_NULL, BIG_DECIMAL_NOT_NULL, LOCAL_DATE_NOT_NULL, OFFSET_TIME_NOT_NULL, OFFSET_DATE_TIME_NOT_NULL, LOCAL_TIME_NOT_NULL, LOCAL_DATE_TIME_NOT_NULL, DATE_TEST_NOT_NULL, URL_NOT_NULL}

    protected Specification<NotNullScalarsTestEntity> createFilter(NotNullScalarsTestEntityFilter filter) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (filter.getStringNotNull() != null) {
                    predicates.add(criteriaBuilder.notEqual(root.get("stringNotNull"), filter.getStringNotNull()));
                }
                if (filter.getBigIntNotNullMin() != null) {
                    predicates.add(criteriaBuilder.greaterThan(root.get("bigIntNotNull"), filter.getBigIntNotNullMin()));
                }
                if (filter.getBigDecimalNotNullMin() != null) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("bigDecimalNotNull"), filter.getBigDecimalNotNullMin()));
                }
                if (filter.getLocalDateNotNullMax() != null) {
                    predicates.add(criteriaBuilder.lessThan(root.get("localDateNotNull"), filter.getLocalDateNotNullMax()));
                }
                if (filter.getOffsetTimeNotNullMax() != null) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("offsetTimeNotNull"), filter.getOffsetTimeNotNullMax()));
                }
                if (filter.getOffsetDateTimeNotNullMin() != null && filter.getOffsetDateTimeNotNullMax() != null) {
                    predicates.add(criteriaBuilder.between(root.get("offsetDateTimeNotNull"), filter.getOffsetDateTimeNotNullMin(), filter.getOffsetDateTimeNotNullMax()));
                }
                if (filter.getLocalTimeNotNull() != null) {
                    predicates.add(criteriaBuilder.notEqual(root.get("localTimeNotNull"), filter.getLocalTimeNotNull()));
                }
                if (filter.getLocalDateTimeNotNullMin() != null) {
                    predicates.add(criteriaBuilder.greaterThan(root.get("localDateTimeNotNull"), filter.getLocalDateTimeNotNullMin()));
                }
                if (filter.getDateTestNotNullMin() != null) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("dateTestNotNull"), filter.getDateTestNotNullMin()));
                }
                if (filter.getUrlNotNull() != null) {
                    predicates.add(criteriaBuilder.notEqual(root.get("urlNotNull"), filter.getUrlNotNull()));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

}