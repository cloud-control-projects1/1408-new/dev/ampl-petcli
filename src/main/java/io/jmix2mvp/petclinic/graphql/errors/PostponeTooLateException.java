package io.jmix2mvp.petclinic.graphql.errors;

public class PostponeTooLateException extends RuntimeException {
    public PostponeTooLateException(String message) {
        super(message);
    }
}
