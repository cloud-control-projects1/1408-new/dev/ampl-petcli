package io.jmix2mvp.petclinic.graphql;

import io.jmix2mvp.petclinic.dto.VisitDTO;
import io.jmix2mvp.petclinic.entity.Pet;
import io.jmix2mvp.petclinic.entity.Visit;
import io.jmix2mvp.petclinic.repository.PetRepository;
import io.jmix2mvp.petclinic.repository.VisitRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureHttpGraphQlTester;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.graphql.ResponseError;
import org.springframework.graphql.test.tester.HttpGraphQlTester;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDateTime;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureHttpGraphQlTester
@WithMockUser(authorities = {ADMIN})
public class VisitControllerTest {

    @Autowired
    private HttpGraphQlTester graphQlTester;

    @Autowired
    private VisitRepository visitRepository;
    @Autowired
    private PetRepository petRepository;
    private Pet pet;


    @BeforeEach
    public void setup() {
        pet = new Pet();
        pet.setIdentificationNumber("V1245");
        petRepository.save(pet);
    }

    @AfterEach
    void tearDown() {
        visitRepository.deleteAll();
        petRepository.deleteAll();
    }

    @Test
    public void testPostponeSuccessful() {
        LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);

        Visit visit = new Visit();
        visit.setVisitStart(tomorrow.withHour(10).withMinute(0).withSecond(0));
        visit.setVisitEnd(tomorrow.withHour(10).withMinute(30).withSecond(0));
        visit.setPet(pet);
        visit = visitRepository.save(visit);

        //given: initial data
        Long visitId = visit.getId();
        Integer days = 2;

        graphQlTester.documentName("postponeVisit")
                .variable("visitId", visitId)
                .variable("days", days)
                .execute()
                .path("postponeVisit")
                .entity(VisitDTO.class)
                .satisfies(returnedValue -> {
                    assertEquals(visitId, returnedValue.getId());
                    assertEquals(tomorrow.plusDays(2).getDayOfMonth(), returnedValue.getVisitStart().getDayOfMonth());
                    assertEquals(10, returnedValue.getVisitStart().getHour());
                    assertEquals(0, returnedValue.getVisitStart().getMinute());

                    assertEquals(tomorrow.plusDays(2).getDayOfMonth(), returnedValue.getVisitEnd().getDayOfMonth());
                });

        visit = visitRepository.findById(visitId).orElseThrow();
        assertEquals(tomorrow.plusDays(2).getDayOfMonth(), visit.getVisitStart().getDayOfMonth());
    }

    @Test
    public void testPostponeVisitFromPast() {
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);

        Visit visit = new Visit();
        visit.setVisitStart(yesterday.withHour(11).withMinute(0).withSecond(0));
        visit.setVisitEnd(yesterday.withHour(12).withMinute(0).withSecond(0));
        visit.setPet(pet);
        visit = visitRepository.save(visit);

        //given: initial data
        Long visitId = visit.getId();

        graphQlTester.documentName("postponeVisitOneDay")
                .variable("visitId", visitId)
                .execute()
                .errors()
                .satisfy(responseErrors -> {
                    assertEquals(1, responseErrors.size());
                    ResponseError er = responseErrors.get(0);
                    assertEquals(er.getErrorType().toString(), "BAD_REQUEST");
                    assertTrue(er.getMessage() != null && er.getMessage().contains("already completed"));
                });
    }
}
