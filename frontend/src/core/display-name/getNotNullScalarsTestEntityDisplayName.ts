import { NotNullScalarsTestEntity } from "../../gql/graphql";

export function getNotNullScalarsTestEntityDisplayName(
  entityInstance?: NotNullScalarsTestEntity | null
): string {
  if (entityInstance == null) {
    return "";
  }
  if (entityInstance.id != null) {
    return String(entityInstance.id);
  }
  return JSON.stringify(entityInstance);
}
