import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Breadcrumb } from "antd";
import { ClientValidationTestEntityCards } from "./ClientValidationTestEntityCards";
import { ClientValidationTestEntityCardsEditor } from "./ClientValidationTestEntityCardsEditor";
import { BreadcrumbContext } from "../../../core/screen/BreadcrumbContext";
import { usePageTitle } from "../../../core/screen/usePageTitle";
import { useIntl } from "react-intl";

export function ClientValidationTestEntityCardsScreenLayout() {
  const intl = useIntl();
  usePageTitle(
    intl.formatMessage({ id: "screen.ClientValidationTestEntityCards" })
  );

  const { recordId } = useParams();
  const [breadcrumbItems, setBreadcrumbItems] = useState<string[]>([]);

  return (
    <>
      {recordId && (
        <Breadcrumb className="crud-screen-breadcrumb">
          {breadcrumbItems.map((item, index) => (
            <Breadcrumb.Item key={`breadcrumb${index}`}>{item}</Breadcrumb.Item>
          ))}
        </Breadcrumb>
      )}

      <BreadcrumbContext.Provider value={setBreadcrumbItems}>
        <div style={{ display: recordId ? "none" : "block" }}>
          <ClientValidationTestEntityCards />
        </div>
        {recordId && (
          <ClientValidationTestEntityCardsEditor
            refetchQueries={["Get_Client_Validation_Test_Entity_List"]}
          />
        )}
      </BreadcrumbContext.Provider>
    </>
  );
}
