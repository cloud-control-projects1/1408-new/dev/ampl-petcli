import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Breadcrumb } from "antd";
import { OwnerTableWithPage } from "./OwnerTableWithPage";
import { OwnerTableWithPageEditor } from "./OwnerTableWithPageEditor";
import { BreadcrumbContext } from "../../../core/screen/BreadcrumbContext";
import { usePageTitle } from "../../../core/screen/usePageTitle";
import { useIntl } from "react-intl";

export function OwnerTableWithPageScreenLayout() {
  const intl = useIntl();
  usePageTitle(intl.formatMessage({ id: "screen.OwnerTableWithPage" }));

  const { recordId } = useParams();
  const [breadcrumbItems, setBreadcrumbItems] = useState<string[]>([]);

  return (
    <>
      {recordId && (
        <Breadcrumb className="crud-screen-breadcrumb">
          {breadcrumbItems.map((item, index) => (
            <Breadcrumb.Item key={`breadcrumb${index}`}>{item}</Breadcrumb.Item>
          ))}
        </Breadcrumb>
      )}

      <BreadcrumbContext.Provider value={setBreadcrumbItems}>
        <div style={{ display: recordId ? "none" : "block" }}>
          <OwnerTableWithPage />
        </div>
        {recordId && (
          <OwnerTableWithPageEditor
            refetchQueries={["Get_Owner_List_With_Pagination"]}
          />
        )}
      </BreadcrumbContext.Provider>
    </>
  );
}
