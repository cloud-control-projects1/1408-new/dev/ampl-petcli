import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Breadcrumb } from "antd";
import { ScalarsCardsWithFilterSortPage } from "./ScalarsCardsWithFilterSortPage";
import { ScalarsCardsWithFilterSortPageEditor } from "./ScalarsCardsWithFilterSortPageEditor";
import { BreadcrumbContext } from "../../../core/screen/BreadcrumbContext";
import { usePageTitle } from "../../../core/screen/usePageTitle";
import { useIntl } from "react-intl";

export function ScalarsCardsWithFilterSortPageScreenLayout() {
  const intl = useIntl();
  usePageTitle(
    intl.formatMessage({ id: "screen.ScalarsCardsWithFilterSortPage" })
  );

  const { recordId } = useParams();
  const [breadcrumbItems, setBreadcrumbItems] = useState<string[]>([]);

  return (
    <>
      {recordId && (
        <Breadcrumb className="crud-screen-breadcrumb">
          {breadcrumbItems.map((item, index) => (
            <Breadcrumb.Item key={`breadcrumb${index}`}>{item}</Breadcrumb.Item>
          ))}
        </Breadcrumb>
      )}

      <BreadcrumbContext.Provider value={setBreadcrumbItems}>
        <div style={{ display: recordId ? "none" : "block" }}>
          <ScalarsCardsWithFilterSortPage />
        </div>
        {recordId && (
          <ScalarsCardsWithFilterSortPageEditor
            refetchQueries={[
              "Get_Scalars_Test_Entity_List_With_Filter_Page_Sort"
            ]}
          />
        )}
      </BreadcrumbContext.Provider>
    </>
  );
}
